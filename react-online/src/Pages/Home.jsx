import React from 'react';
import SlickSlider from '../Components/Slider';
import { HomeSlider } from '../Data/Sliders';
import ProductsLoop from '../Components/ProductsLoop';

function Home( props ) {
	return (
		<>
			<SlickSlider sliders={HomeSlider}/>
			<ProductsLoop/>
		</>
	);
}

export default Home;
