import AboutUs from '../Pages/AboutUs';
import Blog from '../Pages/Blog';
import Contact from '../Pages/Contact';
import Home from '../Pages/Home';
import Category from '../Pages/Category';

export const HeaderMenu = [
	{
		name: 'Про нас',
		path: '/about-us',
		element: <AboutUs/>,
		index: false
	},
	{
		name: 'Статтi',
		path: '/blog',
		element: <Blog/>,
		index: false
	},
	{
		name: 'Контакти',
		path: '/contact',
		element: <Contact/>,
		index: false
	},
];

export const Pages = [
	{
		name: 'Home',
		path: '/',
		element: <Home/>,
		index: true
	}
];

export const ModalMenu = [
	{
		name: 'Отримати онлайн консультацiю',
		path: '/contact',
		element: <Contact/>,
		index: false
	},
	{
		name: 'Записатися на прийом',
		path: '/contact',
		element: <Contact/>,
		index: false
	},
];

export const MainMenu = [
	{
		name: 'Догляд за волоссям',
		path: '/category/:id',
		element: <Category/>,
		index: false
	},
	{
		name: 'Вітаміни/Бади',
		path: '/category/:id',
		element: <Category/>,
		index: false
	},
	{
		name: 'Тіло',
		path: '/category/:id',
		element: <Category/>,
		index: false
	},
	{
		name: 'Косметика для обличчя',
		path: '/category/:id',
		element: <Category/>,
		index: false
	},
	{
		name: 'Для чоловiкiв',
		path: '/category/:id',
		element: <Category/>,
		index: false
	},
];

export const allRouters = [ ...HeaderMenu, ...Pages, ...ModalMenu, ...MainMenu ];
