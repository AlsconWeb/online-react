import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import { createBrowserRouter, createRoutesFromElements, Route, RouterProvider } from 'react-router-dom';
import { allRouters } from './Data/NavMenuData';
import Layout from './Components/Layout';

const root = ReactDOM.createRoot( document.getElementById( 'root' ) );

const router = createBrowserRouter(
	createRoutesFromElements(
		<Route element={<Layout/>}>
			{allRouters.map( ( route, index ) => (
				<Route path={route.path} index={route.index} key={index} element={route.element}/>
			) )}
		</Route>
	)
);

root.render(
	<StrictMode>
		<RouterProvider router={router}>
			<App/>
		</RouterProvider>
	</StrictMode>
);
