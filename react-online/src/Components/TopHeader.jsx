import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import ContactsAndSocials from '../Ui/ContactsAndSocials';

function TopHeader( { robotSchedule, contactsAndSocial } ) {
	console.log( robotSchedule );
	return (
		<div className="top-header">
			<Container>
				<Row className="row justify-content-space-between">
					<Col>
						<p>{robotSchedule}</p>
					</Col>
					<Col>
						<ContactsAndSocials items={contactsAndSocial}/>
					</Col>
				</Row>
			</Container>
		</div>
	);
}

export default TopHeader;
