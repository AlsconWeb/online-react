import React from 'react';
import TopHeader from './TopHeader';
import { contactsAndSocial, robotSchedule } from '../Data/topHeaderData';
import { Col, Container, Row } from 'react-bootstrap';
import NavMenu from '../Ui/NavMenu';
import Logo from '../Ui/Logo';
import { HeaderMenu, MainMenu, ModalMenu } from '../Data/NavMenuData';
import Cart from '../Ui/Cart';

function Header( props ) {
	return (
		<header>
			<TopHeader robotSchedule={robotSchedule} contactsAndSocial={contactsAndSocial}/>
			<Container>
				<Row className="row align-items-center">
					<Col xs={5}>
						<NavMenu menu={HeaderMenu}/>
					</Col>
					<Col xs={2}>
						<Logo/>
					</Col>
					<Col xs={5}>
						<Row className="row align-items-center">
							<Col>
								<NavMenu menu={ModalMenu}/>
							</Col>
							<Col className="col-auto">
								<Cart/>
							</Col>
						</Row>
					</Col>
				</Row>
			</Container>
			<Container>
				<Row>
					<Col className="col-12">
						<NavMenu menu={MainMenu}/>
					</Col>
				</Row>
			</Container>
		</header>

	);
}

export default Header;
