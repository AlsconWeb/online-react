import React from 'react';

function SlickSliderItem( { itemData } ) {
	return (
		<div className="item">
			<div className="description">
				<h5 className="meta">{itemData.meta}</h5>
				<h2 className="title">{itemData.title}</h2>
				<p className="desc">{itemData.description}</p>
				<a className="button mini" href={itemData.buttonUrl}>{itemData.buttonText}</a>
			</div>
			<div className="img">
				<img src={itemData.image} alt="#"/>
			</div>
		</div>
	);
}

export default SlickSliderItem;
