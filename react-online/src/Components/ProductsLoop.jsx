import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Title from '../Ui/Title';
import { Products } from '../Data/ProductData';
import ProductCard from './ProductCard';

function ProductsLoop( props ) {
	return (
		<div className="product">
			<Container>
				<Row>
					<Col className="col-12">
						<Title tag="h2" text="Топ товари"/>
					</Col>
				</Row>
				<Row className={'row row-cols-xl-4 row-cols-lg-3 row-cols-md-2 row-cols-sm-2 row-cols-1'}>
					{
						Products.map( ( item, i ) => <ProductCard key={i} item={item}/> )
					}
				</Row>
			</Container>
		</div>
	);
}

export default ProductsLoop;
