import React from 'react';
import { Col } from 'react-bootstrap';

function ProductCard( { item } ) {
	console.log( item );
	return (
		<Col>
			<div className={'item'}>
				<div className="img">
					<a className="link" href="#"></a>
					{/*<img src="img/product-1.png" alt="#"/>*/}
					<p className="meta top">{item.meta}</p>
				</div>
				<div className="description">
					<h2 className="title">
						<a href="#">
							{item.name}
						</a>
					</h2>
					<p className="availability">{item.stock}</p>
					<p className="price">{item.price}</p>
				</div>
			</div>
		</Col>
	);
}

export default ProductCard;
