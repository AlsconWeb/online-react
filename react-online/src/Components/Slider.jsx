import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Slider from 'react-slick';
import SlickSliderItem from './SlickSliderItem';

function SlickSlider( { sliders } ) {
	
	const sliderSettings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1
	};

	return (
		<div className="banner">
			<Container>
				<Row>
					<Col className="col-12">
						<div className="banner-slider">
							<Slider {...sliderSettings}>
								{
									sliders.map( ( item, i ) => <SlickSliderItem itemData={item} key={i}/> )
								}
							</Slider>
						</div>
					</Col>
				</Row>
			</Container>
		</div>
	);
}

export default SlickSlider;
