import React from 'react';
import { Link } from 'react-router-dom';


function NavMenu( { menu } ) {
	return (
		<>
			<div className="burger"><span></span><span></span><span></span></div>
			<ul className="menu">
				{
					menu.map( ( el, i ) => {
							return (
								<li key={i}>
									<Link to={el.path}>{el.name}</Link>
								</li>
							);
						}
					)
				}
			</ul>
		</>
	);
}

export default NavMenu;
