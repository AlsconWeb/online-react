import React from 'react';

function Cart() {
	return (
		<a className="icon-bag" href="#">
			Кошик
			<p id="mini-cart-count"></p>
		</a>
	);
}

export default Cart;
