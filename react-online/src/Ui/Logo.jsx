import React from 'react';
import { LogoUrl } from '../Data/Images';

function Logo() {
	return (
		<a href="/">
			<img src={LogoUrl.default} alt=""/>
		</a>
	);
}

export default Logo;
