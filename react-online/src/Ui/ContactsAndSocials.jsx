import React from 'react';

function ContactsAndSocials( { items } ) {
	return (
		<ul>
			{
				items.map(
					( el, i ) => {
						return (
							<li key={i} className={el.className}>
								{
									'phone' === el.type ? <a href={'tel:' + el.value}>{el.value}</a> :
										<a href={el.value}></a>
								}
							</li>
						);
					}
				)
			}
		</ul>
	);
}

export default ContactsAndSocials;
