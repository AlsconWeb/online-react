import React from 'react';

function Title( { tag, text, className = 'title', link = null } ) {
	switch ( tag ) {
		case 'h1':
			return (
				<h1 className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</h1> )
				;
		case 'h2':
			return (
				<h2 className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</h2>
			);
		case 'h3':
			return (
				<h3 className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</h3>
			);
		case 'h4':
			return (
				<h4 className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</h4>
			);
		case 'h5':
			return (
				<h5 className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</h5>
			);
		case 'h6':
			return (
				<h5 className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</h5>
			);
		case 'p':
			return (
				<p className={className}>
					{link ? <a href={link}>{text}</a> : <span>{text}</span>}
				</p>
			);

	}
}

export default Title;
